\section{Теорема вириала}

В этом разделе мы попробуем обобщить результаты про среднюю энергию из раздела \ref{sec:energy} на случай
более сложной зависимости потенциальной энергии от расстояния. Нашим основным случаем будет $E_{\Pi} \sim
r^n$.

Рассмотрим систему из $N$ частиц. Запишем ее кинетическую энергию.

\[
    E_{K} = \frac{1}{2} \sum\limits_{k = 1}^{N} m v_i^2 = \frac{1}{2} \sum\limits_k \mathbf{p_k}
    \dv{\mathbf{r_k}}{t}
\]

Воспользуемся формулой производной произведения, чтобы вынести знак прозведения наружу.

\begin{equation}
    \label{eq:avg_kin}
    E_{K} = \frac{1}{2} \dv{}{t} \left( \sum\limits_k \mathbf{p_k} \cdot \mathbf{r_k} \right) -
    \frac{1}{2} \sum\limits_{k} \mathbf{F_k} \cdot \mathbf{r_k}
\end{equation}

\begin{definition}
    Величину $\sum\limits_{k} \mathbf{F_k} \cdot \mathbf{r_k}$ называют \textbf{вириалом} системы из $N$
    частиц.
\end{definition}

Пользуясь выражением \ref{eq:avg_kin} мы докажем следующую теорему.

\begin{theorem}{Теорема <<вириала>> (Клаузиус 1870)}{}
    Если движение системы <<финитно>> (т.е. все скорости и координаты конечны), а силы потенциальны, то:
    
    $$ 2 \avg{E_{K}} = - \avg{\sum\limits_{k} \mathbf{F_k} \cdot \mathbf{r_k}},$$
    где среднее берется по достаточно большому промежутку времени.
\end{theorem}

\begin{proof}
    Для доказательства теоремы рассмотрим большой промежуток времени $T$. Предположим, что
    $\avg{\dv{}{t} \sum\limits_k \mathbf{p_k} \cdot \mathbf{r_k}} \ge \delta$ (случай $\le -\delta$
    рассматривается аналогично), где $\delta > 0$ не стремится к $0$ с ростом $T$. Но это означает, что
    $\sum\limits_k \mathbf{p_k} \cdot \mathbf{r_k} \to \infty$, что противоречит финитности движения.

    Таким образом утверждение теоремы слудет из выражения (\ref{eq:avg_kin}).
\end{proof}


Вернемся к нашему случаю: пусть все частицы взаимодействуют между собой с потенциалом $U \sim
r^n$.

\begin{equation}
	\mathbf{F}_k = \sum\limits_{j = 1}^N \mathbf{F}_{kj} 
\end{equation}
где $\mathbf{F}_{kj}$~--- сила, действующая на $k$-ю частицу со стороны $j$-й. Запишем вириал данной системы:
\begin{equation}
    \sum\limits_{k = 1}^N \mathbf{F}_k \cdot \mathbf{r}_k =
    \sum\limits_{k = 1}^N \sum\limits_{j = 1}^N \mathbf{F}_{kj} \cdot \mathbf{r}_k =
    \sum\limits_{k = 1}^N \sum\limits_{j < k} \mathbf{F}_{kj} \cdot \mathbf{r}_k +
    \sum\limits_{k = 1}^N \sum\limits_{j > k} \mathbf{F}_{kj} \cdot \mathbf{r}_k.
\end{equation}
    
Поменяем в одном слагаемом индексы суммирования воспользовавшись третьим законом Ньютона ($F_{jk} = -
F_{kj}$):
\begin{equation}
    \label{eq:ver-sum}
    \sum\limits_{k = 1}^N \mathbf{F}_k \cdot \mathbf{r}_k =
    \sum\limits_{k = 1}^N \sum_{j < k} \mathbf{F}_{kj} \cdot (\mathbf{r}_k  - \mathbf{r}_j). 
\end{equation}

Теперь мы можем воспользоваться тем фактом, что силы потенциальны, а также нашим предположением
$U_{kj}(r) \sim r^n$, в таком случае сила~--- производная по расстоянию потенциальной энегрии со знаком
минус:

\begin{equation}
    \mathbf{F}_{kj} = - \dv{U_{kj}}{r} \cdot \frac{\mathbf{r}_k - \mathbf{r}_j}{r_{jk}} 
\end{equation}

Подставим это в (\ref{eq:ver-sum})
\begin{equation}
    \sum\limits_{k = 1}^N \mathbf{F}_k \cdot \mathbf{r}_k = -\sum\limits_{k = 1}^N \sum\limits_{j < k}
    \dv{U_{kj}}{r} \cdot \frac{(\mathbf{r}_k - \mathbf{r}_j)^2}{r_{jk}} =
    -\sum\limits_{k = 1}^N \sum\limits_{j < k} \dv{U_{kj}}{r} r_{jk} = -\sum\limits_{k = 1}^{N}
    \sum\limits_{j < k} n U_{kj} = -n E_{\Pi}.
\end{equation}

Давайте строго сформулируем доказаное утверждение.

\begin{corollary}
    Если система из большого числа частиц стабильна, и ее движение финитно, а взаимодействие между парами
    частиц потенциально и имеет зависимость $U \sim r^n$, товыполнено следующее соотношение:
    \[
        2 \avg{E_{K}} = n \avg{E_{\Pi}}.
    \]    
\end{corollary}

С частным случаем этой теоремы мы уже сталкивались, когда считали средние кинетическую и потенциальную
энергию осциллятора. Тогда взаимодействие имело квадратичную зависимость энергии от расстояния, то есть
$n = 2$, значит $\avg{E_{K}} = \avg{E_{\Pi}}$. Что соответствует предсказаниям теоремы вириала.  
