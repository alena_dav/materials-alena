#!/bin/bash
set -e

mkdir -p buildresult

if [ "$1" == "--help" ]; then
    echo """Usage: `basename $0` [DIRECTORY]
Attempts to build all *.tex files in specified directory with pdflatex.
Files that don't contain \"end{document}\" are presumed to be
included fragments and skipped.
Exits on first error."""
	exit 0
fi

WHERE=$1
if [ -z "$WHERE" ] ; then
	WHERE=.
fi

STARTDIR=`pwd`
for i in `find "$WHERE" -name '*.tex'`; do
	DIR=`dirname "$i"`
	FILE=`basename "$i"`
	grep -q 'end{document}' "$i" || continue
	cd "$DIR"
	echo "Building $i"
	#if ! pdflatex --output-directory $STARTDIR/buildresult -halt-on-error -interaction=nonstopmode "$FILE" >/dev/null; then
    #            pdflatex --output-directory $STARTDIR/buildresult -halt-on-error -interaction=nonstopmode "$FILE" || true
    if ! pdflatex -halt-on-error -interaction=nonstopmode "$FILE" >/dev/null; then
                pdflatex -halt-on-error -interaction=nonstopmode "$FILE" || true
		echo "Error building $i"
		exit 1
        fi
	cd "$STARTDIR"
done

find "$WHERE" \( -name '*.aux' -o -name '*.log' -o -name '*.out' \) -delete

echo "All files built successfully"
